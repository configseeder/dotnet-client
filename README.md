# ConfigSeeder .NET Client Documentation

This project was created with [.NET Core](https://dotnet.microsoft.com/download/dotnet-core) version 2.1.

## Development server

To run the client locally, you can just start a debugging session in the IIS / IIS Express by binding the host & port to anything you want.

## Continuous Integration

This project supports continuous integration in [GitLab](https://gitlab.com/). You can check out the CI/CD configuration in the `.gitlab-ci.yml` file. The build artifacts will be stored separately for different build steps. Current pipeline includes the build itself & a NuGet package generation.

## Further help

To get more help on the .NET go check out the [.NET Learning Center](https://dotnet.microsoft.com/learn).
