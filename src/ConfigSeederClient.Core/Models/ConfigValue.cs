﻿using ConfigSeederClient.Core.Enums;
using NodaTime;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConfigSeederClient.Core.Models
{
    public class ConfigValue
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public ConfigurationValueType Type { get; set; }
        public string ConfigurationValueId { get; set; }
        public bool Secured { get; set; }
        public string Environment { get; set; }
        public string ValidFrom { get; set; }
        public string ValidTo { get; set; }
        public string VersionFrom { get; set; }
        public string VersionTo { get; set; }
        public string Context { get; set; }
        public long LastUpdateInMilliseconds { get; set; }
        public string LastZonedUpdate { get; set; }
    }
}
