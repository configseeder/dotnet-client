﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConfigSeederClient.Core.Models
{
    public class Identification
    {
        public static string HEADER_X_USER_AGENT = "X-User-Agent";
        public static string HEADER_X_HOSTNAME = "X-Hostname";
        public static string HEADER_X_HOST_IDENTIFICATION = "X-Host-Identification";

        /// <summary>
        /// Create identification object based on type.
        /// </summary>
        /// <param name="clientType">client type (e.g. Core, Spring, Maven, EclipseMicroProfileConfig)</param>
        /// <returns>identification</returns>
        public static Identification Create(string clientType)
        {
            // TODO: add real identity when implementing authentication
            return new Identification();
        }

        public string UserAgent { get; set; }
        public string HostName { get; set; }
        public string HostIdentification { get; set; }
    }
}
