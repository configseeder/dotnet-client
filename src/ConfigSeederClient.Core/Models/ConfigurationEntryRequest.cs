﻿using ConfigSeederClient.Core.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConfigSeederClient.Core.Models
{
    public class ConfigurationEntryRequest
    {
        public string configurationGroupKey;
        public int? configurationGroupVersionNumber; // can be empty
        public VersionedConfigurationGroupSelectionMode? selectionMode; // can be empty

        /// <summary>
        /// validates the entry.
        /// </summary>
        /// <param name="index"></param>
        public void Validate(int index)
        {
            if (string.IsNullOrEmpty(configurationGroupKey))
            {
                throw new InvalidClientConfigurationException("Field configurationGroupKey at index " + index + " should not be null or empty");
            }
            if (configurationGroupVersionNumber != null && (selectionMode != null && selectionMode != VersionedConfigurationGroupSelectionMode.VERSION_NUMBER))
            {
                throw new InvalidClientConfigurationException("Search by version number should not have selection mode set at index " + index);
            }
        }
    }
}
