﻿using System;
using System.Collections.Generic;

namespace ConfigSeederClient.Core.Models
{
    public class ConfigurationRequest
    {
        /** Default tenant key. */
        public readonly static string DEFAULT_TENANT_KEY = "default";

        /** Tenant key is optional, fallback to "default". */
        public string TenantKey { get; set; } = DEFAULT_TENANT_KEY;

        /** Environment key is mandatory. */
        public string EnvironmentKey { get; set; }

        /** Context is optional. */
        public string Context { get; set; }

        /** Version is optional. */
        public string Version { get; set; }

        /** Date time is optional. */
        public DateTime? DateTime { get; set; }

        public List<ConfigurationEntryRequest> Configurations { get; set; }

        /// <summary>
        /// C'tor for serialization.
        /// </summary>
        public ConfigurationRequest()
        {
            Configurations = new List<ConfigurationEntryRequest>();
        }

        /// <summary>
        /// Validates the DTO.
        /// </summary>
        public void Validate()
        {
            if (TenantKey == null)
            {
                throw new InvalidClientConfigurationException("tenantKey should never be null");
            }
            if (string.IsNullOrEmpty(EnvironmentKey))
            {
                throw new InvalidClientConfigurationException("environment should either be null or have a value (length > 0)");
            }
            if (Configurations == null || Configurations.Count == 0)
            {
                throw new InvalidClientConfigurationException("configurations should have at least one element");
            }
            int i = 0;
            foreach (ConfigurationEntryRequest configurationGroup in Configurations)
            {
                configurationGroup.Validate(i++);
            }
        }
    }
}
