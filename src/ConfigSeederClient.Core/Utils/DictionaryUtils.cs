﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigSeederClient.Core.Utils
{
    public static class DictionaryUtils
    {
        public static IEnumerable<string> GetNextDotIndexOrZero(this Dictionary<object, object> properties, string groupPrefix, int startIndex)
        {
            return properties
                .Where(x => x.Key.ToString().StartsWith(groupPrefix))
                .Select(x => x.Key.ToString().Substring(startIndex, ((x.Key.ToString().IndexOf('.', startIndex) - startIndex) < 0 ? 0 : x.Key.ToString().IndexOf('.', startIndex) - startIndex)))
                .Distinct();
        }

        public static string[] GetConfigurationValuesByPrefixKey(this Dictionary<object,object> properties,string groupPrefix)
        {
           return properties.Where(x => x.Key.ToString().StartsWith(groupPrefix))
                .Select(x => x.Value.ToString()).Distinct().ToArray();
        }
    }
}
