﻿using Microsoft.Extensions.Configuration;
using System;
using System.Configuration;

namespace ConfigSeederClient.Core.Utils
{
    /// <summary>
    /// Helps reading values from the system environment.
    /// </summary>
    public static class ParameterUtil
    {
        /// <summary>
        /// Reads a parameter from system property and if not present from environment.
        /// </summary>
        /// <param name="key"></param>
        /// <returns>configuration value if found in one of the system or env properties</returns>
        public static string GetParameter(string key)
        {
            // configuration contains system property and environment
            var configuration = GetConfig();

            // system property
            var systemParameter = configuration[key];
            if (systemParameter != null)
            {
                return systemParameter.Trim();
            }

            // environment
            var environmentParameter = configuration[SnakeCase(key)];
            if (environmentParameter != null)
            {
                return environmentParameter.Trim();
            }
            return null;
        }
        public static string SnakeCase(string key)
        {
            return key
                .Replace("-", "")
                .Replace(".", "_")
                .ToUpper();
        }

        /// <summary>
        /// Сreated a configuration object using ConfigurationBuilder from explicitly specified configuration sources
        /// </summary>
        public static IConfiguration GetConfig()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables();

            return builder.Build();
        }
    }
}
