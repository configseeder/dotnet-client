﻿using System.Collections.Generic;
using System.IO;
using YamlDotNet.Serialization;

namespace ConfigSeederClient.Shared.Converter
{
    public class YamlToDictionaryConverter
    {
        public Dictionary<object, object> Convert(Stream yamlStream)
        {
            using (var reader = new StreamReader(yamlStream))
            {
                var deserializer = new Deserializer();
                var propertiesObject = deserializer.Deserialize<object>(reader);
                Dictionary<object, object> properties = new Dictionary<object, object>();

                ExtractRedirect(null, properties, propertiesObject); 

                return properties;
            }
        }

        private void ExtractRedirect(string path, Dictionary<object, object> properties, object obj)
        {
            if (obj == null)
            {
                return;
            }
            if (obj.GetType() == typeof(Dictionary<object, object>))
            {
                Extract(path, properties, (Dictionary<object, object>)obj);
            }
            else if (obj.GetType() == typeof(List<object>))
            {
                Extract(path, properties, (List<object>)obj);
            }
            else
            {
                Extract(path, properties, obj);
            }
        }

        private void Extract(string path, Dictionary<object, object> properties, object value)
        {
            properties.Add(path, value);
        }

        private void Extract(string path, Dictionary<object, object> properties, Dictionary<object, object> pairs)
        {
            string basePath = path == null ? string.Empty : path + ".";
            foreach (var pair in pairs)
            {
                ExtractRedirect(basePath + pair.Key.ToString(), properties, pair.Value);
            }
        }

        private void Extract(string path, Dictionary<object, object> properties, List<object> list)
        {
            object[] entries = list.ToArray();
            string basePath = path == null ? string.Empty : path + ".";
            for (int i = 0; i < entries.Length; i++)
            {
                ExtractRedirect(basePath + i, properties, entries[i]);
            }
        } 
    }
}
