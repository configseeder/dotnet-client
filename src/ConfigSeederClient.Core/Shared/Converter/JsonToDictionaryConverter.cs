﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;

namespace ConfigSeederClient.Core.Shared.Converter
{
    /// <summary>
    /// Helps to convert settings json to Dictionary.
    /// </summary>
    public class JsonToDictionaryConverter
    {
        public Dictionary<object, object> Convert(Stream jsonStream)
        {
            using (var reader = new StreamReader(jsonStream))
            {
                string json = reader.ReadToEnd();

                Dictionary<object, object> nodes = new Dictionary<object, object>();
                JObject rootObject = JObject.Parse(json);

                ParseJson(rootObject, nodes);
                return nodes;
            }
        }

        private static bool ContainsArray(JToken containerToken)
        {
            if (containerToken.Type == JTokenType.Object)
            {
                foreach (JProperty child in containerToken.Children<JProperty>())
                {
                    if (child.Type == JTokenType.Array ||
                        child.Value.Type == JTokenType.Array)
                    {
                        return true;
                    }
                    ContainsArray(child.Value);
                }
            }
            else if (containerToken.Type == JTokenType.Array)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Parse a JSON object and return it as a dictionary of strings with keys showing the hierarchy.
        /// </summary>
        /// <param name = "token"></param>
        /// <param name = "nodes"></param>
        /// <param name = "parentLocation"></param>
        /// <returns></returns>
        static bool ParseJson(JToken token, Dictionary<object, object> nodes, string parentLocation = "")
        {
            if (token.HasValues)
            {
                foreach (JToken child in token.Children())
                {
                    if (token.Type == JTokenType.Property)
                    {
                        if (parentLocation == "")
                        {
                            parentLocation = ((JProperty)token).Name;
                        }
                        else
                        {
                            parentLocation += "." + ((JProperty)token).Name;
                        }
                    }

                    if (token.Type == JTokenType.Array)
                    {
                        var i = 1;
                        foreach (var j in (JArray)token)
                        {
                            foreach(var prop in ((JObject)j).Properties())
                            {
                                var key = parentLocation + ".group" + i + "." + (object)(prop.Name);
                                var value = (object)prop.Value.ToString();
                                if (!nodes.ContainsKey(key))
                                {
                                    nodes.Add(key, value);
                                }
                            }
                            i++;
                        }
                    }
                    else
                    {
                        ParseJson(child, nodes, parentLocation);
                    }
                }

                // we are done parsing and this is a parent node
                return true;
            }
            else
            {
                // leaf of the tree
                if (nodes.ContainsKey(parentLocation))
                {
                    // this was an array
                    nodes[parentLocation] += "|" + token.ToString();
                }
                else
                {
                    // this was a single property
                    nodes.Add(parentLocation, token.ToString());
                }

                return false;
            }
        }
    }
}
