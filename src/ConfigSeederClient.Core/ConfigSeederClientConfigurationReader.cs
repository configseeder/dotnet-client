﻿using ConfigSeederClient.Core.Enums;
using ConfigSeederClient.Core.Models;
using ConfigSeederClient.Core.Shared.Converter;
using ConfigSeederClient.Core.Utils;
using ConfigSeederClient.Shared.Converter;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ConfigSeederClient.Core
{
    public static class ConfigSeederClientConfigurationReader
    {
        /// <summary>
        /// Reads configuration model from settings json and system parameters.
        /// </summary>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static ConfigSeederClientConfiguration FromJson(Stream jsonInputStream)
        {
            if(jsonInputStream == null)
            {
                throw new InvalidClientConfigurationException("Could not find configuration");
            }
            var properties = new JsonToDictionaryConverter().Convert(jsonInputStream);
            return FromMap(properties);
        }
        /// <summary>
        /// Reads configuration model from yaml and system parameters.
        /// </summary>
        /// <param name="yamlInputStream"></param>
        /// <returns>configuration</returns>
        public static ConfigSeederClientConfiguration FromYaml(Stream yamlInputStream)
        {
            if (yamlInputStream == null)
            {
                throw new InvalidClientConfigurationException("Could not find configuration");
            }
            var properties = new YamlToDictionaryConverter().Convert(yamlInputStream);
            return FromMap(properties);
        }

        public static ConfigSeederClientConfiguration FromMap(Dictionary<object, object> properties)
        {
            var configSeederClientConfiguration = new ConfigSeederClientConfiguration();

            configSeederClientConfiguration.Disabled =
                    FetchConfig(properties, ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_DISABLED, false);
            configSeederClientConfiguration.InitializationMode =
                FetchConfig(properties, ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_INITIALIZATIONMODE, ConfigSeederClientConfiguration.DEFAULT_INITIALIZATION_MODE);
            configSeederClientConfiguration.RefreshMode =
                    FetchConfig(properties, ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_REFRESHMODE, ConfigSeederClientConfiguration.DEFAULT_REFRESH_MODE);
            configSeederClientConfiguration.ServerUrl =
                    FetchConfig(properties, ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_SERVERURL);
            configSeederClientConfiguration.ApiKey =
                    FetchConfig(properties, ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_APIKEY);
            configSeederClientConfiguration.RefreshCycle =
                    FetchConfig(properties, ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_REFRESHCYCLE, ConfigSeederClientConfiguration.DEFAULT_REFRESH_CYCLE);
            configSeederClientConfiguration.RetryWaitTime =
                    FetchConfig(properties, ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_RETRYWAITTIME, ConfigSeederClientConfiguration.DEFAULT_RETRY_WAIT_TIME);
            configSeederClientConfiguration.MaxRetries =
                    FetchConfig(properties, ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_MAXRETRIES, ConfigSeederClientConfiguration.DEFAULT_MAX_RETRIES);
            configSeederClientConfiguration.ConnectionTimeout =
                    FetchConfig(properties, ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_CONNECTIONTIMEOUT, ConfigSeederClientConfiguration.DEFAULT_CONNECTION_TIMEOUT);
            configSeederClientConfiguration.ReadTimeout =
                    FetchConfig(properties, ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_READTIMEOUT, ConfigSeederClientConfiguration.DEFAULT_READ_TIMEOUT);

            var requestConfiguration = configSeederClientConfiguration.requestConfiguration;
            requestConfiguration.EnvironmentKey =
                    FetchConfig(properties, ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_ENVIRONMENTKEY);
            requestConfiguration.TenantKey =
                    FetchConfig(properties, ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_TENANTKEY, ConfigurationRequest.DEFAULT_TENANT_KEY);
            requestConfiguration.Version =
                    FetchConfig(properties, ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_VERSION);
            requestConfiguration.Context =
                    FetchConfig(properties, ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_CONTEXT);
            requestConfiguration.DateTime =
                    FetchConfig<DateTime?>(properties, ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_DATETIME, null);

            var configurationRequest = configSeederClientConfiguration.requestConfiguration;

            var configGroupsShortVersion = FetchConfig(properties, ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_CONFIGURATIONGROUPKEYS);
            if (configGroupsShortVersion != null)
            {
                configurationRequest.Configurations = ReadSimpleConfigurationRequestEntities(configGroupsShortVersion.Split(","));
            }
            else if (properties.ContainsKey(ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_CONFIGURATIONGROUPKEYS + ".0"))
            {
                configurationRequest.Configurations = ReadSimpleConfigurationRequestEntities(properties);
            }
            else
            {
                configurationRequest.Configurations = ReadConfigurationRequestEntities(properties);
            }

            return configSeederClientConfiguration;
        }

        private static List<ConfigurationEntryRequest> ReadSimpleConfigurationRequestEntities(string[] values)
        {
            return values.Select(value => new ConfigurationEntryRequest
            {
                configurationGroupKey = value.Trim(),
                selectionMode = VersionedConfigurationGroupSelectionMode.LATEST
            }).ToList();
        }

        private static List<ConfigurationEntryRequest> ReadSimpleConfigurationRequestEntities(Dictionary<object, object> properties)
        {
            string groupPrefix = ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_CONFIGURATIONGROUPKEYS + ".";

            string[] configurationGroupKeys = properties.GetConfigurationValuesByPrefixKey(groupPrefix);

            return ReadSimpleConfigurationRequestEntities(configurationGroupKeys);
        }

        private static List<ConfigurationEntryRequest> ReadConfigurationRequestEntities(Dictionary<object, object> properties)
        {
            var configurations = new List<ConfigurationEntryRequest>();

            string groupPrefix = ConfigSeederClientConfiguration.CONFIGSEEDER_CLIENT_CONFIGURATIONS + ".";
            int startIndex = groupPrefix.Length;

            var distinctGroupsNames = properties.GetNextDotIndexOrZero(groupPrefix, startIndex);

            foreach (var group in distinctGroupsNames)
            {
                var keyName = FetchConfig(properties, groupPrefix + group + ".key");
                var selectionMode = FetchConfig(properties, groupPrefix + group + ".selectionMode");
                var version = FetchIntConfig(properties, groupPrefix + group + ".version", null);

                if (keyName == null)
                {
                    throw new InvalidClientConfigurationException("Missing configuration '" + groupPrefix + nameof(group) + ".key'");
                }

                var config = new ConfigurationEntryRequest()
                {
                    configurationGroupKey = keyName
                };

                if (version != null)
                {
                    config.configurationGroupVersionNumber = version;
                    config.selectionMode = VersionedConfigurationGroupSelectionMode.VERSION_NUMBER;
                }
                else if (selectionMode != null)
                {
                    try
                    {
                        config.selectionMode = (VersionedConfigurationGroupSelectionMode)Enum.Parse(typeof(VersionedConfigurationGroupSelectionMode), selectionMode);
                    }
                    catch (Exception)
                    {
                        throw new InvalidClientConfigurationException("Invalid configuration '" + groupPrefix + nameof(group) + ".version'. "
                                                      + "Not a valid value of ["
                                                      + string.Join(",", Enum.GetNames(typeof(VersionedConfigurationGroupSelectionMode)))
                                                      + "].");
                    }
                }
                else
                {
                    config.selectionMode = VersionedConfigurationGroupSelectionMode.LATEST;
                }
                configurations.Add(config);
            }
            return configurations;
        }

        private static T FetchConfig<T>(Dictionary<object, object> source, string key, T defaultValue)
        {
            source.TryGetValue(key, out object resultObj);

            string parameterOrEnvironmentValue = ParameterUtil.GetParameter(key);
            if (parameterOrEnvironmentValue != null)
            {
                resultObj = parameterOrEnvironmentValue;
            }

            if (resultObj != null)
            {
                if (int.TryParse(resultObj.ToString(), out int num))
                {
                    resultObj = num;
                }
                if (defaultValue != null && defaultValue.GetType().BaseType == typeof(Enum))
                {
                    return (T)Enum.Parse(typeof(T), resultObj.ToString());
                }
                return (T)resultObj;
            }
            else
            {
                return defaultValue;
            }
        }

        private static string FetchConfig(Dictionary<object, object> source, string key)
        {
            return FetchConfig(source, key, null);
        }

        private static string FetchConfig(Dictionary<object, object> source, string key, string defaultValue)
        {
            return FetchConfig<string>(source, key, defaultValue);
        }

        private static int? FetchIntConfig(Dictionary<object, object> source, string key, int? defaultValue)
        {
            return FetchConfig(source, key, defaultValue);
        }
    }
}
