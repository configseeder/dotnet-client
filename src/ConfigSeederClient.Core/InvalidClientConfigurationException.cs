﻿using System;

namespace ConfigSeederClient.Core
{
    public class InvalidClientConfigurationException : Exception
    {
        public InvalidClientConfigurationException()
        {
        }
        public InvalidClientConfigurationException(string technicalMessage)
            : base(technicalMessage)
        {
        }

        public InvalidClientConfigurationException(string technicalMessage, Exception throwable)
            : base(technicalMessage, throwable)
        {
        }
    }
}
