﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace ConfigSeederClient.Core
{
    public class HttpException : Exception
    {
        public HttpException(int httpStatusCode)
        {
            HttpStatusCode = httpStatusCode;
        }

        public HttpException(HttpStatusCode httpStatusCode)
        {
            HttpStatusCode = (int)httpStatusCode;
        }

        public HttpException(int httpStatusCode, string message) : base(message)
        {
            HttpStatusCode = httpStatusCode;
        }

        public HttpException(HttpStatusCode httpStatusCode, string message) : base(message)
        {
            HttpStatusCode = (int)httpStatusCode;
        }

        public HttpException(int httpStatusCode, string message, Exception inner) : base(message, inner)
        {
            HttpStatusCode = httpStatusCode;
        }

        public HttpException(HttpStatusCode httpStatusCode, string message, Exception inner) : base(message, inner)
        {
            HttpStatusCode = (int)httpStatusCode;
        }

        public int StatusCode { get { return HttpStatusCode; } }

        public int HttpStatusCode { get; }
    }
}
