﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConfigSeederClient.Core
{
    /// <summary>
    /// Technical Exceptions thrown by ConfigSeeder. Thrown if an inconsistent state occurs.
    /// </summary>
    class ConfigSeederTechnicalException : SystemException
    {
        public ConfigSeederTechnicalException()
        {
        }
        /// <summary>
        /// Constructs a new exception with the specified detail message.
        /// </summary>
        /// <param name="message"></param>
        public ConfigSeederTechnicalException(string message)
            : base(message)
        {
        }
        /// <summary>
        /// Constructs a new exception with the specified detail message and
        /// cause.  <p>Note that the detail message associated with
        /// {@code cause} is <i>not</i> automatically incorporated in
        /// this exception's detail message.      
        /// </summary>
        /// <param name="message"></param>
        /// <param name="throwable"></param>
        public ConfigSeederTechnicalException(string message, Exception throwable)
            : base(message, throwable)
        {
        }
    }
}
