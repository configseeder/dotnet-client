﻿using ConfigSeederClient.Core.Enums;
using ConfigSeederClient.Core.Models;
using System.Collections.Generic;
using System.IO;

namespace ConfigSeederClient.Core
{
    public class ConfigSeederClientConfiguration
    {
        /** Default initialization mode. */
        public readonly static InitializationModes DEFAULT_INITIALIZATION_MODE = InitializationModes.EAGER_ASYNC;
        /** Defreadonly ault refresh mode. */
        public readonly static RefreshModes DEFAULT_REFRESH_MODE = RefreshModes.TIMER;
        /** Defreadonly ault refresh cycle in milliseconds. */
        public readonly static int DEFAULT_REFRESH_CYCLE = 60000;
        /** Defreadonly ault max retries after failing the synchronization. */
        public readonly static int DEFAULT_MAX_RETRIES = 3;
        /** Defreadonly ault retry wait time after failure in milliseconds. */
        public readonly static int DEFAULT_RETRY_WAIT_TIME = 1000;
        /** Defreadonly ault connection timeout in milliseconds. */
        public readonly static int DEFAULT_CONNECTION_TIMEOUT = 1000;
        /** Defreadonly ault read timeout in milliseconds. */
        public readonly static int DEFAULT_READ_TIMEOUT = 2000;

        /** CONFIG_SEEDER_CLIENT_PREFIX. */
        private readonly static string PREFIX = "configseeder.client.";
        /** Con readonly fig Key for Config Seeder Server URL. */
        public readonly static string CONFIGSEEDER_CLIENT_SERVERURL = PREFIX + "serverUrl";
        /** Con readonly fig Key for API Key. */
        public readonly static string CONFIGSEEDER_CLIENT_APIKEY = PREFIX + "apiKey";
        /** Con readonly fig Key for Tenant Key. */
        public readonly static string CONFIGSEEDER_CLIENT_TENANTKEY = PREFIX + "tenantKey";
        /** Con readonly fig Key for Context Filter. */
        public readonly static string CONFIGSEEDER_CLIENT_CONTEXT = PREFIX + "context";
        /** Con readonly fig Key for Date Time Filter. */
        public readonly static string CONFIGSEEDER_CLIENT_DATETIME = PREFIX + "dateTime";
        /** Con readonly fig Key for Version Filter. */
        public readonly static string CONFIGSEEDER_CLIENT_VERSION = PREFIX + "version";
        /** Con readonly fig Key for Environment Filter. */
        public readonly static string CONFIGSEEDER_CLIENT_ENVIRONMENTKEY = PREFIX + "environmentKey";
        /** Con readonly fig Key for Configuration Filter (simple config). */
        public readonly static string CONFIGSEEDER_CLIENT_CONFIGURATIONGROUPKEYS = PREFIX + "configurationGroupKeys";
        /** Con readonly fig Key for Configuration Filter. */
        public readonly static string CONFIGSEEDER_CLIENT_CONFIGURATIONS = PREFIX + "configurations";
        /** Con readonly fig Key for Connection Timeout. */
        public readonly static string CONFIGSEEDER_CLIENT_CONNECTIONTIMEOUT = PREFIX + "connectionTimeout";
        /** Con readonly fig Key for Read Timeout. */
        public readonly static string CONFIGSEEDER_CLIENT_READTIMEOUT = PREFIX + "readTimeout";
        /** Con readonly fig Key for Max Retries. */
        public readonly static string CONFIGSEEDER_CLIENT_MAXRETRIES = PREFIX + "maxRetries";
        /** Con readonly fig Key for Retry Wait Time. */
        public readonly static string CONFIGSEEDER_CLIENT_RETRYWAITTIME = PREFIX + "retryWaitTime";
        /** Con readonly fig Key for refresh cycle. */
        public readonly static string CONFIGSEEDER_CLIENT_REFRESHCYCLE = PREFIX + "refreshCycle";
        /** Con readonly fig Key for Refresh mode of config seeder client. */
        public readonly static string CONFIGSEEDER_CLIENT_REFRESHMODE = PREFIX + "refreshMode";
        /** Con readonly fig Key for Initialization mode of config seeder client. */
        public readonly static string CONFIGSEEDER_CLIENT_INITIALIZATIONMODE = PREFIX + "initializationMode";
        /** Con readonly fig Key for Disable configseeder client. */
        public readonly static string CONFIGSEEDER_CLIENT_DISABLED = PREFIX + "disabled";


        /** All configurations. */
        public readonly static List<string> ALL_CONFIG_KEYS = new List<string>()
        {
            CONFIGSEEDER_CLIENT_SERVERURL,
            CONFIGSEEDER_CLIENT_APIKEY,
            CONFIGSEEDER_CLIENT_TENANTKEY,
            CONFIGSEEDER_CLIENT_CONTEXT,
            CONFIGSEEDER_CLIENT_DATETIME,
            CONFIGSEEDER_CLIENT_VERSION,
            CONFIGSEEDER_CLIENT_ENVIRONMENTKEY,
            CONFIGSEEDER_CLIENT_CONFIGURATIONGROUPKEYS,
            CONFIGSEEDER_CLIENT_CONFIGURATIONS,
            CONFIGSEEDER_CLIENT_CONNECTIONTIMEOUT,
            CONFIGSEEDER_CLIENT_READTIMEOUT,
            CONFIGSEEDER_CLIENT_MAXRETRIES,
            CONFIGSEEDER_CLIENT_RETRYWAITTIME,
            CONFIGSEEDER_CLIENT_REFRESHCYCLE,
            CONFIGSEEDER_CLIENT_REFRESHMODE,
            CONFIGSEEDER_CLIENT_INITIALIZATIONMODE,
            CONFIGSEEDER_CLIENT_DISABLED
        };

        public bool Disabled { get; set; } = false;
        public InitializationModes? InitializationMode { get; set; } = DEFAULT_INITIALIZATION_MODE;
        public RefreshModes? RefreshMode { get; set; } = DEFAULT_REFRESH_MODE;
        public string ServerUrl { get; set; }
        public string ApiKey { get; set; }
        public int? RefreshCycle { get; set; } = DEFAULT_REFRESH_CYCLE;
        public int? RetryWaitTime { get; set; } = DEFAULT_RETRY_WAIT_TIME;
        public int? MaxRetries { get; set; } = DEFAULT_MAX_RETRIES;
        public int? ConnectionTimeout { get; set; } = DEFAULT_CONNECTION_TIMEOUT;
        public int? ReadTimeout { get; set; } = DEFAULT_READ_TIMEOUT;

        public ConfigurationRequest requestConfiguration = new ConfigurationRequest();

        public string UserAgent { get; set; } = "ConfigSeederClient-Core";
        public string ConfigurationGroupKeys { get; set; }

        /// <summary>
        /// Read configurations from json file.
        /// </summary>
        /// <param name="jsonFile"></param>
        /// <returns>ConfigSeederClientConfiguration</returns>
        public static ConfigSeederClientConfiguration FromJson(string jsonFile)
        {
            if (jsonFile == null)
            {
                throw new InvalidClientConfigurationException("Configuration file path cannot be empty");
            }
            using (var jsonFileStream = new FileStream(jsonFile, FileMode.Open, FileAccess.Read))
            {
                return FromJson(jsonFileStream);
            }
        }
        /// <summary>
        /// Read configurations from json file.
        /// </summary>
        /// <param name="configuration"></param>
        /// <returns>ConfigSeederClientConfiguration</returns>
        public static ConfigSeederClientConfiguration FromJson(Stream jsonInputStream)
        {
            return ConfigSeederClientConfigurationReader.FromJson(jsonInputStream);
        }


        /// <summary>
        /// Read configurations from yaml file.
        /// </summary>
        /// <param name="yamlFile"></param>
        /// <returns>ConfigSeederConfigurationModel</returns>
        public static ConfigSeederClientConfiguration FromYaml(string yamlFile)
        {
            if (string.IsNullOrEmpty(yamlFile))
            {
                throw new InvalidClientConfigurationException("Configuration file path cannot be empty");
            }
            using (var yamlFileStream = new FileStream(yamlFile, FileMode.Open, FileAccess.Read))
            {
                return FromYaml(yamlFileStream);
            }
        }

        public static ConfigSeederClientConfiguration FromYaml(Stream yamlFileStream)
        {
            return ConfigSeederClientConfigurationReader.FromYaml(yamlFileStream);
        }

        public void Validate()
        {
            if (Disabled)
            {
                return;
            }
            if (InitializationMode == null)
            {
                throw new InvalidClientConfigurationException("initialization mode should not be null");
            }
            if (RefreshMode == null)
            {
                throw new InvalidClientConfigurationException("refreshMode should not be null");
            }
            if (RefreshMode != RefreshModes.MANUAL && RefreshCycle == null)
            {
                throw new InvalidClientConfigurationException("refreshCycle not defined");
            }
            if (ServerUrl == null || !ServerUrl.StartsWith("http"))
            {
                throw new InvalidClientConfigurationException("serverUrl not well defined");
            }
            if (MaxRetries == null)
            {
                throw new InvalidClientConfigurationException("maxRetries not defined");
            }
            if (RetryWaitTime == null)
            {
                throw new InvalidClientConfigurationException("retryWaitTime not defined");
            }
            if (ApiKey == null)
            {
                throw new InvalidClientConfigurationException("apiKey not defined");
            }
            if (ConnectionTimeout == null)
            {
                throw new InvalidClientConfigurationException("connectionTimeout unknown");
            }
            if (ReadTimeout == null)
            {
                throw new InvalidClientConfigurationException("readTimeout unknown");
            }
            if (requestConfiguration == null)
            {
                throw new InvalidClientConfigurationException("requestConfiguration not defined");
            }
            requestConfiguration.Validate();
        }
    }
}
