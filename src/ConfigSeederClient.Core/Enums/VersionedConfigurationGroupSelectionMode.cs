﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConfigSeederClient.Core.Enums
{
    public enum VersionedConfigurationGroupSelectionMode
    {

        /** Latest versioned configuration group. */
        LATEST,

        /** Latest released versioned configuration group. */
        LATEST_RELEASED,

        /** By exact version number.*/
        VERSION_NUMBER

    }
}
