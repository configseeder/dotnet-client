﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConfigSeederClient.Core.Enums
{
    public enum InitializationModes
    {
        /** On initialization immediately loads values. */
        EAGER,

        /** On initialization immediately loads values, but asynchronously. */
        EAGER_ASYNC,

        /** Only loads on first request. */
        LAZY
    }
}
