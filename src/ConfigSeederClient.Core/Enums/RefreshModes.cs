﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConfigSeederClient.Core.Enums
{
    public enum RefreshModes
    {
        /** Only manual triggered refresh. */
        MANUAL,

        /** Only on request, but blocking. */
        LAZY,

        /** Refresh values with a background worker. */
        TIMER,

        /** Never refreshed. */
        NEVER
    }
}
