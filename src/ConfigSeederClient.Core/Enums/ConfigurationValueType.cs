﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConfigSeederClient.Core.Enums
{
    public enum ConfigurationValueType
    {
        /** Single line string. */
        STRING,

        /** String with multiline character. */
        MULTILINE_STRING,

        /** Support for regular expressions. */
        REGEX,

        /** Support for enumerations. */
        ENUM,

        /** Textual representation of an integer. */
        INTEGER,

        /** Textual representation of any number. */
        NUMBER,

        /** Textual representation of a boolean. */
        BOOLEAN,

        /** ConfigurationGroupId stored as comma separated string. */
        CONFIGURATION_GROUP,

        /** API Key Type. */
        API_KEY_TYPE
    }
}
