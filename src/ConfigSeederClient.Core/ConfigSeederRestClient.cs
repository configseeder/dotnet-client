﻿using ConfigSeederClient.Core.Enums;
using ConfigSeederClient.Core.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NodaTime;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConfigSeederClient.Core
{
    public class ConfigSeederRestClient
    {
        public static string HEADER_ACCEPT = "Accept";
        public static string HEADER_CONTENT_TYPE = "Content-Type";
        public static string HEADER_AUTHORIZATION = "Authorization";

        public static string PATH_TO_EXTERNAL_PROPERTIES = "/public/api/v1/configurations";
        public static string APPLICATION_JSON_METADATA = "application/metadata+json";

        private readonly string _url;
        private readonly int _connectionTimeout;
        private readonly int _readTimeout;
        private readonly string _apiKey;
        private readonly Identification _identification;

        public ConfigSeederRestClient(string url, string apiKey, int connectionTimeout = 500, int readTimeout = 2000, Identification identification = null)
        {
            if (identification == null)
            {
                identification = Identification.Create("Core");
            }

            _connectionTimeout = connectionTimeout;
            _readTimeout = readTimeout;
            _identification = identification;
            if (!url.StartsWith("http"))
            {
                throw new ArgumentException("URL '" + url + "' seems not be a valid url");
            }
            if (url.EndsWith("/"))
            {
                _url = url.Substring(0, url.Length - 1);
            }
            else
            {
                _url = url;
            }
            if (apiKey.Trim().Length == 0)
            {
                throw new ArgumentException("API Key should not be null");
            }
            _apiKey = apiKey;
        }

        public async Task<Dictionary<string, ConfigValue>> GetProperties(ConfigurationRequest configurationRequest, CancellationToken token)
        {
            configurationRequest.Validate();

            var requestUrl = GetRequestUrl();
            var requestBody = BuildRequestBody(configurationRequest);

            var timeoutCancellation = CancellationTokenSource.CreateLinkedTokenSource(token);
            timeoutCancellation.CancelAfter(_connectionTimeout);

            try
            {
                var siteUrl = new Uri(requestUrl);
                using (var httpClient = new HttpClient())
                {
                    httpClient.Timeout = TimeSpan.FromMilliseconds(_readTimeout);

                    httpClient.DefaultRequestHeaders.Add(HEADER_ACCEPT, APPLICATION_JSON_METADATA);
                    httpClient.DefaultRequestHeaders.Add(HEADER_AUTHORIZATION, "Bearer " + _apiKey);

                    httpClient.DefaultRequestHeaders.Add(Identification.HEADER_X_USER_AGENT, _identification.UserAgent);
                    httpClient.DefaultRequestHeaders.Add(Identification.HEADER_X_HOSTNAME, _identification.HostName);
                    httpClient.DefaultRequestHeaders.Add(Identification.HEADER_X_HOST_IDENTIFICATION, _identification.HostIdentification);

                    var stringContent = new StringContent(requestBody, Encoding.UTF8, APPLICATION_JSON_METADATA);

                    var responseMessage = await httpClient.PostAsync(siteUrl, stringContent, timeoutCancellation.Token).ConfigureAwait(false);
                    if (!responseMessage.IsSuccessStatusCode)
                    {
                        var errorMsg = await responseMessage.Content.ReadAsStringAsync();
                        throw new HttpException(responseMessage.StatusCode, errorMsg);
                    }

                    var resultContent = await responseMessage.Content.ReadAsStringAsync();
                    var rootObject = JArray.Parse(resultContent);
                    var values = new Dictionary<string, ConfigValue>();
                    foreach (var content in rootObject.Children<JObject>())
                    {
                        var configValue = new ConfigValue();

                        configValue.Value = content.GetValue("value")?.ToString();
                        configValue.Type = (ConfigurationValueType)Enum.Parse(typeof(ConfigurationValueType), content.GetValue("type")?.ToString()); //ConfigurationValueType 

                        configValue.LastUpdateInMilliseconds = content.GetValue("lastUpdateInMilliseconds") == null ?
                            DateTime.Now.Millisecond
                            : long.Parse(content.GetValue("lastUpdateInMilliseconds")?.ToString());

                        var lastUpdate = content.GetValue("lastUpdate");
                        if (lastUpdate == null)
                        {
                            var now = DateTime.Now;
                            var instant = Instant.FromUtc(now.Year, now.Month, now.Day, now.Hour, now.Minute);
                            var timeZone = DateTimeZoneProviders.Tzdb.GetSystemDefault();
                            var localNow = new ZonedDateTime(instant, timeZone);
                            configValue.LastZonedUpdate = localNow.ToString();
                        }
                        else
                        {
                            configValue.LastZonedUpdate = content.GetValue("lastUpdate")?.ToString();
                        }
                        configValue.Key = content.GetValue("key")?.ToString();
                        values.Add(configValue.Key, configValue);
                    }
                    return values;
                }
            }
            catch (JsonReaderException e)
            {
                throw new ConfigSeederTechnicalException("Could not decode to UTF-8", e);
            }
            catch (UriFormatException e)
            {
                throw new InvalidClientConfigurationException("URL '" + requestUrl + "' is invalid", e);
            }
            catch (OperationCanceledException e)
            when (!token.IsCancellationRequested && timeoutCancellation.IsCancellationRequested)
            {
                throw new TimeoutException("Could not connect to " + requestUrl + " within " + _connectionTimeout + "msec.", e);
            }
            catch (IOException e)
            {
                throw new ConfigSeederTechnicalException("Could not connect and fetch values", e);
            }
            finally
            {
                timeoutCancellation.Dispose();
            }
        }

        public string GetRequestUrl()
        {
            return _url + PATH_TO_EXTERNAL_PROPERTIES;
        }

        private string BuildRequestBody(ConfigurationRequest configurationRequest)
        {
            var jsonObject = new JObject
            {
                { "tenantKey", configurationRequest.TenantKey },
                { "configurations", ToJsonValue(configurationRequest.Configurations) }
            };

            if (!string.IsNullOrEmpty(configurationRequest.EnvironmentKey))
            {
                jsonObject.Add("environmentKey", configurationRequest.EnvironmentKey);
            }
            if (!string.IsNullOrEmpty(configurationRequest.Version))
            {
                jsonObject.Add("version", configurationRequest.Version);
            }
            if (!string.IsNullOrEmpty(configurationRequest.Context))
            {
                jsonObject.Add("context", configurationRequest.Context);
            }
            if (configurationRequest.DateTime != null)
            {
                jsonObject.Add("dateTime", configurationRequest.DateTime);
            }
            else
            {
                jsonObject.Add("dateTime", DateTime.Now);
            }
            return jsonObject.ToString();
        }

        private JArray ToJsonValue(List<ConfigurationEntryRequest> configurations)
        {
            JArray jsonArray = new JArray();
            configurations.ForEach(config =>
            {
                JObject configEntry = new JObject();
                if (!string.IsNullOrEmpty(config.configurationGroupKey))
                {
                    configEntry.Add("configurationGroupKey", config.configurationGroupKey);
                }
                if (config.configurationGroupVersionNumber != null)
                {
                    configEntry.Add("configurationGroupVersionNumber", config.configurationGroupVersionNumber);
                }
                if (config.selectionMode != null)
                {
                    configEntry.Add("selectionMode", config.selectionMode.ToString());
                }
                jsonArray.Add(configEntry);
            });
            return jsonArray;
        }
    }
}
