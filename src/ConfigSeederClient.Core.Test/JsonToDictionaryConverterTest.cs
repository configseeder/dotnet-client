﻿using ConfigSeederClient.Core.Shared.Converter;
using NUnit.Framework;
using System.Collections.Generic;
using System.IO;

namespace ConfigSeederClient.Core.Test
{
    [TestFixture]
    public class JsonToDictionaryConverterTest
    {
        [Test]
        public void Should_Be_Able_To_Read_Example_1()
        {
            // given
            var file = Directory.GetCurrentDirectory() + "/resources/read-appsettings-1.json";

            //when
            var jsonToDictionaryConverter = new JsonToDictionaryConverter();
            using (var inputStream = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                var properties = jsonToDictionaryConverter.Convert(inputStream);
                Assert.AreEqual(properties.GetValueOrDefault("base.foo"), "blafoo");
                Assert.AreEqual(properties.GetValueOrDefault("base.bar"), "blabar");
                Assert.AreEqual(properties.GetValueOrDefault("foo"), "nixfoo");
                Assert.AreEqual(properties.GetValueOrDefault("bar"), "nixbar");
            }
        }
        [Test]
        public void Should_Be_Able_To_Read_Example_2()
        {
            // given
            var file = Directory.GetCurrentDirectory() + "/resources/read-appsettings-2.json";

            //when
            var jsonToDictionaryConverter = new JsonToDictionaryConverter();
            using (var inputStream = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                var properties = jsonToDictionaryConverter.Convert(inputStream);
                Assert.AreEqual(properties.GetValueOrDefault("foo"), "Hello");
                Assert.AreEqual(properties.GetValueOrDefault("bar"), "World");
            }
        }
        [Test]
        public void Should_Be_Able_To_Read_Example_3()
        {
            // given
            var file = Directory.GetCurrentDirectory() + "/resources/read-appsettings-3.json";

            //when
            var jsonToDictionaryConverter = new JsonToDictionaryConverter();
            using (var inputStream = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                var properties = jsonToDictionaryConverter.Convert(inputStream);
                Assert.AreEqual(properties.GetValueOrDefault("level1.level2.simple"), "value");
                Assert.AreEqual(properties.GetValueOrDefault("level1.level2.indented.1.a"), "b");
                Assert.AreEqual(properties.GetValueOrDefault("level1.level2.indented.1.c"), "d");
                Assert.IsEmpty(properties.GetValueOrDefault("level1.level2.indented.2").ToString());
                Assert.IsEmpty(properties.GetValueOrDefault("level1.level2.indented.3").ToString());
            }
        }
    }
}
