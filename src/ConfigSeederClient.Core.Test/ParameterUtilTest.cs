﻿using ConfigSeederClient.Core.Utils;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConfigSeederClient.Core.Test
{
    [TestFixture]
    public class ParameterUtilTest
    {
        [Test]
        public void Should_Snake_Well()
        {
            Assert.AreEqual(ParameterUtil.SnakeCase("hello.world"), "HELLO_WORLD");
            Assert.AreEqual(ParameterUtil.SnakeCase("hello-world"), "HELLOWORLD");
            Assert.AreEqual(ParameterUtil.SnakeCase("helloWorld"), "HELLOWORLD");
            Assert.AreEqual(ParameterUtil.SnakeCase("my.parameter-caseNow"), "MY_PARAMETERCASENOW");
        }

        [Test]
        public void Test_Get_Environment_Simple_Variable()
        {
            //given
            Environment.SetEnvironmentVariable("ENV_TEST", " value-with-space", EnvironmentVariableTarget.User);
            //when
            var result = ParameterUtil.GetParameter("ENV_TEST");

            //then
            Assert.AreEqual("value-with-space", result, "ParameterUtil.getParameter()");
        }

        [Test]
        public void Test_Get_Environment_Specific_Variable()
        {
            //given
            Environment.SetEnvironmentVariable("CONFIGSEEDER_CLIENT_TENANTKEY", "democlient", EnvironmentVariableTarget.User);

            //when
            var result = ParameterUtil.GetParameter("configseeder.client.tenantKey");

            //then 
            Assert.AreEqual("democlient", result, "ParameterUtil.getParameter()");
        }
    }
}
