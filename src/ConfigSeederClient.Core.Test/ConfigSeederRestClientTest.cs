﻿using ConfigSeederClient.Core.Enums;
using ConfigSeederClient.Core.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using WireMock.RequestBuilders;
using WireMock.ResponseBuilders;
using WireMock.Server;

namespace ConfigSeederClient.Core.Test
{
    [TestFixture]
    public class ConfigSeederRestClientTest
    {
        public FluentMockServer _server;

        [SetUp]
        public void StartMockServer()
        {
            _server = FluentMockServer.Start();
        }

        [Test]
        public void Should_Fail_For_Missing_Parameters()
        {
            Assert.Throws<ArgumentException>(() => new ConfigSeederRestClient("localhost:8080", "api-key"));
            Assert.Throws<ArgumentException>(() => new ConfigSeederRestClient("localhost:8080", " "));
        }

        [Test]
        public async Task Should_Send_Request_With_All_Params()
        {
            // Assign
            var identification = new Identification()
            {
                UserAgent = "ConfigSeederClientTest",
                HostName = "jupiter",
                HostIdentification = Guid.NewGuid().ToString()
            };
            var configuration = new ConfigurationEntryRequest()
            {
                configurationGroupKey = "groupKey",
                selectionMode = VersionedConfigurationGroupSelectionMode.LATEST
            };
            var configurations = new List<ConfigurationEntryRequest>
            {
                configuration
            };

            var request = new ConfigurationRequest()
            {
                TenantKey = "test-tenant",
                EnvironmentKey = "prod",
                Version = "1.9.0",
                DateTime = new DateTime(2019, 2, 15, 7, 5, 0),
                Context = "development",
                Configurations = configurations
            };

            var content = "[{\"key\": \"foo.bar\", \"value\": \"any text\", \"type\": \"STRING\", "
                        + "\"lastUpdate\":\"2018-01-03T17:36:01\", \"lastUpdateInMilliseconds\": 125455545445}]";

            _server
              .Given(Request.Create().WithPath(ConfigSeederRestClient.PATH_TO_EXTERNAL_PROPERTIES)
                        .WithHeader(ConfigSeederRestClient.HEADER_CONTENT_TYPE, ConfigSeederRestClient.APPLICATION_JSON_METADATA + "; charset=UTF-8")
                        .WithHeader(ConfigSeederRestClient.HEADER_ACCEPT, ConfigSeederRestClient.APPLICATION_JSON_METADATA)
                        .WithHeader(ConfigSeederRestClient.HEADER_AUTHORIZATION, "Bearer some-api-key")
                        .WithHeader(Identification.HEADER_X_USER_AGENT, identification.UserAgent)
                        .WithHeader(Identification.HEADER_X_HOSTNAME, identification.HostName)
                        .WithHeader(Identification.HEADER_X_HOST_IDENTIFICATION, identification.HostIdentification)
                        .UsingPost())
              .RespondWith(
                Response.Create()
                .WithHeader("Content-Type", "application/json")
                 .WithStatusCode(200)
                  .WithBody(content)
              );
            var restClient = new ConfigSeederRestClient(_server.Urls[0], "some-api-key", 5000, 5000, identification);

            CancellationTokenSource cancelTokenSource = new CancellationTokenSource();
            CancellationToken token = cancelTokenSource.Token;
            // Act
            var configurationValues = await restClient.GetProperties(request, token);

            // Assert
            Assert.AreEqual(configurationValues.GetValueOrDefault("foo.bar").Value, "any text");

            cancelTokenSource.Cancel();
        }

        [TearDown]
        public void ShutdownServer()
        {
            _server.Stop();
        }
    }
}
