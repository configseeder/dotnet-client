﻿using ConfigSeederClient.Core.Enums;
using ConfigSeederClient.Core.Models;
using NUnit.Framework;
using System.IO;
using System.Linq;

namespace ConfigSeederClient.Core
{
    [TestFixture]
    class ConfigSeederClientConfigurationReaderTest
    {
        [Test]
        public void Should_Read_From_Json_Settings()
        {
            //given
            var file = Directory.GetCurrentDirectory() + "/resources/appsettings.json";

            // when
            var configuration = ConfigSeederClientConfiguration.FromJson(file);

            //then
            VerifyFullContent(configuration);
        }

        [Test]
        public void Should_Read_From_Yaml_Full()
        {
            // given
            var file = Directory.GetCurrentDirectory() + "/resources/configseeder-full.yaml";

            // when
            var configuration = ConfigSeederClientConfiguration.FromYaml(file);

            // then
            VerifyFullContent(configuration);
        }

        [Test]
        public void Should_Read_From_Yaml_Minimal_1()
        {
            // given
            var file = Directory.GetCurrentDirectory() + "/resources/configseeder-minimal-1.yaml";

            // when
            ConfigSeederClientConfiguration configuration = ConfigSeederClientConfiguration.FromYaml(file);

            // then
            VerifyDefaultsAndMinimalContent(configuration);
        }

        [Test]
        public void Should_Read_From_Yaml_Minimal_2()
        {
            // given
            var file = Directory.GetCurrentDirectory() + "/resources/configseeder-minimal-2.yaml";

            // when
            ConfigSeederClientConfiguration configuration = ConfigSeederClientConfiguration.FromYaml(file);

            // then
            VerifyDefaultsAndMinimalContent(configuration);
        }
        private void VerifyDefaultsAndMinimalContent(ConfigSeederClientConfiguration configuration)
        {
            Assert.AreEqual(configuration.InitializationMode, ConfigSeederClientConfiguration.DEFAULT_INITIALIZATION_MODE, nameof(configuration.InitializationMode));
            Assert.AreEqual(configuration.RefreshMode, ConfigSeederClientConfiguration.DEFAULT_REFRESH_MODE, nameof(configuration.InitializationMode));
            Assert.AreEqual(configuration.MaxRetries, ConfigSeederClientConfiguration.DEFAULT_MAX_RETRIES, nameof(configuration.MaxRetries));
            Assert.AreEqual(configuration.RetryWaitTime, ConfigSeederClientConfiguration.DEFAULT_RETRY_WAIT_TIME, nameof(configuration.RetryWaitTime));
            Assert.AreEqual(configuration.RefreshCycle, ConfigSeederClientConfiguration.DEFAULT_REFRESH_CYCLE, nameof(configuration.RefreshCycle));
            Assert.AreEqual(configuration.ServerUrl, "http://localhost:8080", nameof(configuration.ServerUrl));
            Assert.AreEqual(configuration.ApiKey, "any-api-key", nameof(configuration.ApiKey));

            ConfigurationRequest configurationRequest = configuration.requestConfiguration;
            Assert.AreEqual(configurationRequest.EnvironmentKey, "PROD", nameof(configurationRequest.EnvironmentKey));
            Assert.AreEqual(configurationRequest.TenantKey, "democlient", nameof(configurationRequest.TenantKey));
            Assert.IsNull(configurationRequest.Context, nameof(configurationRequest.Context));
            Assert.IsNull(configurationRequest.Version, nameof(configurationRequest.Version));
            Assert.AreEqual(configurationRequest.Configurations.Count, 2, nameof(configurationRequest.Configurations));

            ConfigurationEntryRequest group1 = GetEntryWithGroupName(configurationRequest, "config-group-1");
            Assert.AreEqual(group1.configurationGroupKey, "config-group-1", nameof(group1.configurationGroupKey));
            Assert.AreEqual(group1.selectionMode, VersionedConfigurationGroupSelectionMode.LATEST, nameof(group1.selectionMode));
            Assert.IsNull(group1.configurationGroupVersionNumber, nameof(group1.configurationGroupVersionNumber));

            ConfigurationEntryRequest group2 = GetEntryWithGroupName(configurationRequest, "config-group-2");
            Assert.AreEqual(group2.configurationGroupKey, "config-group-2", nameof(group1.configurationGroupKey));
            Assert.AreEqual(group2.selectionMode, VersionedConfigurationGroupSelectionMode.LATEST, nameof(group1.selectionMode));
            Assert.IsNull(group2.configurationGroupVersionNumber, nameof(group1.configurationGroupVersionNumber));
        }
        private void VerifyFullContent(ConfigSeederClientConfiguration configuration)
        {
            Assert.AreEqual(configuration.InitializationMode, InitializationModes.LAZY, nameof(configuration.InitializationMode));
            Assert.AreEqual(configuration.RefreshMode, RefreshModes.LAZY, nameof(configuration.InitializationMode));
            Assert.AreEqual(configuration.MaxRetries, 5, nameof(configuration.MaxRetries));
            Assert.AreEqual(configuration.RetryWaitTime, 234, nameof(configuration.RetryWaitTime));
            Assert.AreEqual(configuration.RefreshCycle, 5000, nameof(configuration.RefreshCycle));
            Assert.AreEqual(configuration.ServerUrl, "https://demo.configseeder.com", nameof(configuration.ServerUrl));
            Assert.AreEqual(configuration.ApiKey, "any-api-key", nameof(configuration.ApiKey));

            ConfigurationRequest configurationRequest = configuration.requestConfiguration;
            Assert.AreEqual(configurationRequest.EnvironmentKey, "DEV", nameof(configurationRequest.EnvironmentKey));
            Assert.AreEqual(configurationRequest.TenantKey, "democlient", nameof(configurationRequest.TenantKey));
            Assert.AreEqual(configurationRequest.Context, "dotnet-context", nameof(configurationRequest.Context));
            Assert.AreEqual(configurationRequest.Version, "0.0.1-beta", nameof(configurationRequest.Version));

            Assert.AreEqual(configurationRequest.Configurations.Count, 3, nameof(configurationRequest.Configurations));

            ConfigurationEntryRequest group1 = GetEntryWithGroupName(configurationRequest, "config-group-1");
            Assert.AreEqual(group1.configurationGroupKey, "config-group-1", nameof(group1.configurationGroupKey) + nameof(group1));
            Assert.AreEqual(group1.selectionMode, VersionedConfigurationGroupSelectionMode.LATEST, nameof(group1.selectionMode) + nameof(group1));
            Assert.IsNull(group1.configurationGroupVersionNumber, nameof(group1.configurationGroupVersionNumber) + nameof(group1));

            ConfigurationEntryRequest group2 = GetEntryWithGroupName(configurationRequest, "config-group-2");
            Assert.AreEqual(group2.configurationGroupKey, "config-group-2", nameof(group2.configurationGroupKey) + nameof(group2));
            Assert.AreEqual(group2.selectionMode, VersionedConfigurationGroupSelectionMode.LATEST_RELEASED, nameof(group2.selectionMode) + nameof(group2));
            Assert.IsNull(group2.configurationGroupVersionNumber, nameof(group2.configurationGroupVersionNumber) + nameof(group2));

            ConfigurationEntryRequest group3 = GetEntryWithGroupName(configurationRequest, "config-group-3");
            Assert.AreEqual(group3.configurationGroupKey, "config-group-3", nameof(group3.configurationGroupKey) + nameof(group3));
            Assert.AreEqual(group3.selectionMode, VersionedConfigurationGroupSelectionMode.VERSION_NUMBER, nameof(group3.selectionMode) + nameof(group3));
            Assert.AreEqual(group3.configurationGroupVersionNumber, 5, nameof(group3.configurationGroupVersionNumber) + nameof(group3));
        }

        private ConfigurationEntryRequest GetEntryWithGroupName(ConfigurationRequest configurationRequest, string groupKey)
        {
            return configurationRequest.Configurations.FirstOrDefault(x => x.configurationGroupKey == groupKey);
        }
    }
}
