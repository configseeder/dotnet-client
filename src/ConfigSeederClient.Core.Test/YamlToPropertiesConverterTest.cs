﻿using ConfigSeederClient.Shared.Converter;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;

namespace ConfigSeederClient.Core.Test
{
    [TestFixture]
    public class YamlToDictionaryConverterTest
    {
        [Test]
        public void Should_Be_Able_To_Read_Example_1()
        {
            var yamlToPropertiesConverterTest = new YamlToDictionaryConverter();

            var file = Directory.GetCurrentDirectory() + "/resources/read-example-1.yaml";

            using (var inputStream = new FileStream(file,FileMode.Open, FileAccess.Read))
            {
                var properties = yamlToPropertiesConverterTest.Convert(inputStream);
                Assert.AreEqual(properties.GetValueOrDefault("base.0.foo"), "blafoo");
                Assert.AreEqual(properties.GetValueOrDefault("base.0.bar"), "blabar");
                Assert.AreEqual(properties.GetValueOrDefault("base.1.foo"), "nixfoo");
                Assert.AreEqual(properties.GetValueOrDefault("base.1.bar"), "nixbar");
            }
        }

        [Test]
        public void Should_Be_Able_To_Read_Example_2()
        {
            var yamlToPropertiesConverterTest = new YamlToDictionaryConverter();

            var file = Directory.GetCurrentDirectory() + "/resources/read-example-2.yaml";

            using (var inputStream = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                var properties = yamlToPropertiesConverterTest.Convert(inputStream);
                Assert.AreEqual(properties.GetValueOrDefault("bar"), "hello");
                Assert.AreEqual(properties.GetValueOrDefault("foo"), "world");
            }
        }

        [Test]
        public void Should_Be_Able_To_Read_Example_3()
        {
            var yamlToPropertiesConverterTest = new YamlToDictionaryConverter();

            var file = Directory.GetCurrentDirectory() + "/resources/read-example-3.yaml";

            using (var inputStream = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                var properties = yamlToPropertiesConverterTest.Convert(inputStream);
                Assert.AreEqual(properties.GetValueOrDefault("level1.level2.simple"), "value");
                Assert.AreEqual(properties.GetValueOrDefault("level1.level2.indented.0.1.a"), "b");
                Assert.AreEqual(properties.GetValueOrDefault("level1.level2.indented.0.1.c"), "d");
                Assert.AreEqual(Convert.ToInt32(properties.GetValueOrDefault("level1.level2.indented.1")), 2);
                Assert.AreEqual(Convert.ToInt32(properties.GetValueOrDefault("level1.level2.indented.2")), 3);
            }
        }
    }
}
